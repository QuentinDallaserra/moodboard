class Moodboard {

    /* Gets the session stored array
   
       [This method can be used in any template] */

    getArrayPostIds() {

        const localStorageState = localStorage.getItem('moodboardLocalSession') == null 
            ? [] 
            : JSON.parse(localStorage.getItem('moodboardLocalSession'));

        return localStorageState;

    }


    /* Toggle an item by adding/removing its post ID to the 
       moodboard array stored in session, with an optional callback.
   
       [This method can be used in any template] */

    moodboardToggle(itemID, callback) {

        const arrayPostIDS = this.getArrayPostIds();

        const arrayIndex = $.inArray(itemID, arrayPostIDS);

        arrayIndex == -1
            ? arrayPostIDS.push(itemID)
            : arrayPostIDS.splice(arrayIndex, 1);

        localStorage.setItem('moodboardLocalSession', JSON.stringify(arrayPostIDS));

        callback ? callback() : null;

    }


    /* Update the counter with the current number of post IDs stored
       in the moodboard session array.
   
       [This method can be used in any template] */

    updateCounter(element) {

        const arrayPostIDS = this.getArrayPostIds();

        const text = arrayPostIDS.length > 0
            ? (`(${arrayPostIDS.length})`)
            : '';

        element.text(text);

    }


    /* Checks if each element's post ID is stored in the moodboard session
       array and assign a IN/NOT IN class to that element. Callback is optional.
   
       [This method can be used in any template] */

    assignStatus(element, callback) {

        const arrayPostIDS = this.getArrayPostIds();

        element.each(function () {

            const $this = $(this);

            $this.removeClass('moodboard--is-inspired moodboard--not-inspired');

            $.inArray($this.attr('data-post'), arrayPostIDS) > -1
                ? $this.addClass('moodboard--is-inspired')
                : $this.addClass('moodboard--not-inspired');

        }, callback ? callback() : null);

    }


    /* If the current URL doesn't contain a query string, add '?'at the
       end of the URL.
   
       [This method is not to be used in a template] */

    prepareQueryString() {

        if (window.location.href.indexOf('?') === -1) {
            window.history.replaceState(null, null, `${window.location.href}?`);
        }

    }


    /* Updates the URL with a string query containing the post IDs in 
       the current moodboard session array 
   
       [This method is to be used in the Moodboard pinned listing template] */

    updateURL() {

        const arrayPostIDS = this.getArrayPostIds();

        this.prepareQueryString();

        const pageURL = window.location.href.split('?')[0];
        const moodboardPostsIDtoString = arrayPostIDS.join(',').replace(/,/g, '&').split();

        window.history.replaceState(null, null, `${pageURL}?${moodboardPostsIDtoString}`);

    }


    /* Looks for a specific string in the HREF of an element, replaces
       the string with the current URL including it's query string and
       opens it in a new window.
   
       [This method is to be used in the Moodboard pinned listing template] */

    socialSharer(element, link) {

        element.on('click', function (event) {

            event.preventDefault();

            const currentURL = window.location.href.replace(/&/g, '%26');
            const currentHref = $(this).attr('href');
            var newHref = currentHref.replace(link, currentURL);

            window.open(newHref, '_blank');

        });

    }


    /* Checks the post ID count that is in the current moodboard local session 
       array if the session does not contain post IDs the method will return a 'No 
       Results' message else it will return the templateToAppend parameter. 
       NOTE: templateToAppend is optional.
   
       [This method is to be used in the Moodboard pinned listing template] */

    appendResults(container, emptyHtmlToAppend, templateToAppend) {

        const arrayPostIDS = this.getArrayPostIds();

        arrayPostIDS.length > 0
            ? container.append(templateToAppend)
            : container.append(`<div class="moodboard--no-results">${emptyHtmlToAppend}</div>`);

    }


    /* This loads the pinned posts currently in the moodboard session array 
       through a PHP function then calls the updateURL() method. However if 
       a URL with a query string containing post IDs is pasted in the browser, 
       the method will replace the current session array with the post IDs
       pasted in the browser. the method has an optional callback.
       NOTE: the results are passed through the appendResults() method.
   
       [This method is to be used in the Moodboard pinned listing template] */

    loadPosts(container, appendResults, callback) {

        const arrayPostIDS = this.getArrayPostIds();

        const self = this;

        this.prepareQueryString();

        const pageURL = window.location.href.split('?')[0];
        const postsURL = window.location.href.split('?')[1];
        const urlPostIDS = postsURL.split('&');

        container.empty();

        postsURL
            ? localStorage.setItem('moodboardLocalSession', JSON.stringify(urlPostIDS))
            : this.updateURL();

        $.ajax({
            type: 'POST',
            url: site.ajax,
            data: {
                action: 'moodboard_template',
                sessionArray: postsURL
                    ? urlPostIDS
                    : arrayPostIDS
            },
            dataType: 'JSON',
            success: function (response) {
                self.appendResults(container, appendResults, response.template);
            },
            complete: function () {
                callback ? callback() : null;
            },
            error: function (response) {
                console.log('Error');
            }
        });

    }

}
