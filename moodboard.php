<?php

function moodboard_template(){

    $response = [
        'template' => Timber::compile('moodboard.twig', ['posts' => $_POST['sessionArray']]),
    ];

    echo json_encode($response);
    wp_die();
}

add_action('wp_ajax_moodboard_template', 'moodboard_template');
add_action('wp_ajax_nopriv_moodboard_template', 'moodboard_template');
