(function ($, document) {
    'use strict';

    $(document).ready(function () {

        const $moodboardCounter = $('.moodboard__counter');

        if (!$moodboardCounter[0]) {
            return;
        }

        const moodboard = new Moodboard();

        moodboard.updateCounter($moodboardCounter);

    });

})(jQuery, document);
