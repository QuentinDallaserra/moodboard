(function ($, document) {
    'use strict';

    $(document).ready(function () {

        const $moodboardContainer = $('.example-select-page');

        if (!$moodboardContainer[0]) {
            return;
        }

        let canBeLoaded = true;

        const moodboard = new Moodboard();

        const $moodboard = $('.moodboard');
        const $moodboardItem = $('.moodboard__item');
        const $moodboardCounter = $('.moodboard__counter');

        moodboard.assignStatus($moodboardItem);

        $moodboardItem.on('click', function () {

            if (canBeLoaded === true) {

                canBeLoaded = false;

                const $this = $(this);
                const thisPostID = $this.attr('data-post');

                moodboard.moodboardToggle(thisPostID, function () {
                    moodboard.updateCounter($moodboardCounter);
                    moodboard.assignStatus($this, function () {
                        canBeLoaded = true;
                    });
                });

            }

        });

    });

})(jQuery, document);
