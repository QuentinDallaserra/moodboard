(function ($, document) {
    'use strict';

    $(document).ready(function () {

        const $moodboardContainer = $('.example-favourite-page');

        if (!$moodboardContainer[0]) {
            return;
        }

        let canBeLoaded = true;

        const moodboard = new Moodboard();

        const $moodboardCounter = $('.moodboard__counter');
        const $moodboardSharer = $('.moodboard__sharer');
        const noResultsHtml = `No Results`;

        let $moodboardItem = $('.moodboard__item');
        let $moodboard = $('.moodboard');

        moodboard.socialSharer($moodboardSharer, '[jsMoodboardLink]');

        moodboard.loadPosts($moodboardContainer, noResultsHtml, function () {

            $moodboardItem = $('.moodboard__item');
            $moodboard = $('.moodboard');

            moodboard.assignStatus($moodboardItem);
            moodboard.updateCounter($moodboardCounter);

            $moodboardItem.on('click', function () {

                if (canBeLoaded === true) {

                    canBeLoaded = false;

                    const $this = $(this);
                    const thisPostID = $this.attr('data-post');

                    $this.addClass('moodboard--removing');

                    moodboard.moodboardToggle(thisPostID);

                    moodboard.assignStatus($this, function () {

                        $this.addClass('moodboard--removed');
                        $this.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                            $this.remove();
                            moodboard.appendResults($moodboardContainer, noResultsHtml);
                            canBeLoaded = true;
                        });

                    });

                    moodboard.updateCounter($moodboardCounter);
                    moodboard.updateURL();

                }

            });

        });



    });

})(jQuery, document);
